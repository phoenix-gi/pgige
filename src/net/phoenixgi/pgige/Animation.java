package net.phoenixgi.pgige;

/**
 * @author phoenix-gi
 */
public class Animation {

    public int frame;
    public int max;
    public float speed;
    private float count;
    private boolean loop;

    public Animation(int max, float speed) {
        this.max = Math.abs(max);
        frame = 0;
        this.speed = Math.abs(speed);
        count = 0;
        loop = true;
    }

    public void update(float deltaTime) {
        count += deltaTime * speed;
        if (count >= 1) {
            frame++;
            count = 0;
            if (frame >= max) {
                if (loop) {
                    frame = 0;
                } else {
                    frame--;
                }
            }
        }
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}
