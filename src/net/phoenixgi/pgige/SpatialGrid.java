package net.phoenixgi.pgige;

import net.phoenixgi.pgige.object.GameObject;

import java.util.Vector;

/**
 * @author phoenix-gi
 */
public class SpatialGrid {

    Vector[] cells;
    int cellsPerRow;
    int cellsPerColumn;
    float cellSize;
    Vector foundObjects;

    public SpatialGrid(float worldWidth, float worldHeight, float cellSize) {
        this.cellSize = cellSize;
        this.cellsPerRow = (int) Math.ceil(worldWidth / cellSize);
        this.cellsPerColumn = (int) Math.ceil(worldHeight / cellSize);
        int numCells = cellsPerRow * cellsPerColumn;
        cells = new Vector[numCells];
        for (int i = 0; i < numCells; i++) {
            cells[i] = new Vector();
        }
        foundObjects = new Vector();
    }

    public void insertObject(GameObject obj) {
        int[] cellIds = getCellIds(obj);
        int i = 0;
        int cellId = -1;
        while (i < cellIds.length && (cellId = cellIds[i++]) != -1) {
            cells[cellId].addElement(obj);
        }
    }

    public void removeObject(GameObject obj) {
        int[] cellIds = getCellIds(obj);
        int i = 0;
        int cellId = -1;
        while (i < cellIds.length && (cellId = cellIds[i++]) != -1) {
            cells[cellId].removeElement(obj);
        }
    }

    public void clearObjectCells(GameObject obj) {
        for (int i = 0; i < cells.length; i++) {
            cells[i].removeAllElements();
        }
    }

    public Vector getObjectsInCell(int cellId) {
        return cells[cellId];
    }

    public Vector getPotencialColliders(GameObject obj) {
        foundObjects.removeAllElements();
        int[] cellIds = getCellIds(obj);
        int i = 0;
        int cellId = -1;
        while (i < cellIds.length && (cellId = cellIds[i++]) != -1) {
            int len = cells[cellId].size();
            for (int j = 0; j < len; j++) {
                GameObject collider = (GameObject) cells[cellId].elementAt(j);
                if (!foundObjects.contains(collider)) {
                    foundObjects.addElement(collider);
                }
            }
        }
        return foundObjects;
    }

    public int getCellIdForVector(Vector2 pos) {
        int x = (int) Math.floor(pos.x / cellSize);
        int y = (int) Math.floor(pos.y / cellSize);
        if (x >= 0 && x < cellsPerRow && y >= 0 && y < cellsPerColumn) {
            return x + y * cellsPerRow;
        } else {
            return -1;
        }
    }

    public int[] getCellIds(GameObject obj) {
        int[] cellIds = new int[]{-1};
        int x1 = (int) Math.floor(obj.getShape().getLeftX() / cellSize);
        int y1 = (int) Math.floor(obj.getShape().getTopY() / cellSize);
        int x2 = (int) Math.floor((obj.getShape().getRightX()) / cellSize);
        int y2 = (int) Math.floor((obj.getShape().getBottomY()) / cellSize);

        boolean objectInSpaceGrid
                = x1 >= 0 && x1 < cellsPerRow
                && x1 >= 0 && x1 < cellsPerRow
                && y1 >= 0 && y1 < cellsPerColumn
                && y2 >= 0 && y2 < cellsPerColumn;

        if (objectInSpaceGrid) {
            cellIds = new int[(x2 - x1 + 1) * (y2 - y1 + 1)];
            int index = 0;
            for (int x = x1; x <= x2; x++) {
                for (int y = y1; y <= y2; y++) {
                    cellIds[index++] = x + y * cellsPerRow;
                }
            }
        }

        return cellIds;
    }

    public Vector[] getCells() {
        return cells;
    }

    public int getCellsPerRow() {
        return cellsPerRow;
    }

    public int getCellsPerColumn() {
        return cellsPerColumn;
    }

    public void clear() {
        for (Vector cell : cells) {
            cell.clear();
        }
        foundObjects.clear();
    }

}
