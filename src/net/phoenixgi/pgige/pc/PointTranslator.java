package net.phoenixgi.pgige.pc;

public class PointTranslator {

    private float newX;
    private float newY;

    private boolean isTranslateable;

    public PointTranslator(float realX, float realY, float w1, float h1, float w2, float h2) {
        float sy = h1 / h2;
        float sx = w1 / w2;
        float w = w2, h = h2;

        if (sx * h > h1) {
            w = sy * w;
            h = sy * h;
        } else {
            w = sx * w;
            h = sx * h;
        }

        float shiftX = (w1 - w) / 2;
        float shiftY = (h1 - h) / 2;
        isTranslateable = realX >= shiftX && realX <= w + shiftX && realY >= shiftY && realY <= h + shiftY;
        if (isTranslateable) {
            newX = (realX - shiftX) / w * w2;
            newY = (realY - shiftY) / h * h2;
        }
    }

    public float getNewX() {
        return newX;
    }

    public float getNewY() {
        return newY;
    }

    public boolean isTranslateable() {
        return isTranslateable;
    }
}
