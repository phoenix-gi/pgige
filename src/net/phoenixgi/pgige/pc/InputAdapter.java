package net.phoenixgi.pgige.pc;

import javax.swing.*;
import java.awt.event.*;

class InputAdapter implements MouseWheelListener, MouseMotionListener, MouseListener, KeyListener {
    JPanelCanvas canvas;

    public InputAdapter(JPanelCanvas canvas) {
        this.canvas = canvas;
    }

    public StandardInput getInput() {
        return canvas.getInput();
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        if (getInput() != null) {
            getInput().keyTyped(keyEvent);
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (getInput() != null) {
            getInput().keyPressed(keyEvent);
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if (getInput() != null) {
            getInput().keyReleased(keyEvent);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mouseClicked(newE);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mousePressed(newE);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mouseReleased(newE);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mouseEntered(newE);
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mouseExited(newE);
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mouseDragged(newE);
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseEvent newE = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getButton());
                getInput().mouseMoved(newE);
            }
        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (getInput() != null) {
            JPanel p = canvas.panel;
            PointTranslator t = new PointTranslator(e.getX(), e.getY(), p.getWidth(), p.getHeight(), canvas.getWidth(), canvas.getHeight());
            if (t.isTranslateable()) {
                MouseWheelEvent newMouseWheelEvent = new MouseWheelEvent(e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), (int) t.getNewX(), (int) t.getNewY(), e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), e.getScrollType(), e.getScrollAmount(), e.getWheelRotation(), e.getPreciseWheelRotation());
                getInput().mouseWheelMoved(newMouseWheelEvent);
            }
        }
    }
}
