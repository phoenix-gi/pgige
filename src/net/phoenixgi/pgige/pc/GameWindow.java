package net.phoenixgi.pgige.pc;

import net.phoenixgi.pgige.Canvas;
import net.phoenixgi.pgige.Game;

import javax.swing.*;
import java.awt.*;

public class GameWindow extends Game {

    private JFrame frame;
    private JPanelCanvas canvas;

    public GameWindow(JPanelCanvas canvas, String title) {
        this.canvas = canvas;
        buildFrame(title, canvas.getWidth(), canvas.getHeight());
    }

    private void buildFrame(String title, int frameWidth, int frameHeight) {
        frame = new JFrame(title);
        Toolkit tlk = Toolkit.getDefaultToolkit();
        final int screenWidth = tlk.getScreenSize().width;
        final int screenHeight = tlk.getScreenSize().height;
        final int x = (screenWidth - frameWidth) / 2;
        final int y = (screenHeight - frameHeight) / 2;

        frame.setFocusable(false);
        canvas.getPanel().setFocusable(true);
        canvas.getPanel().setPreferredSize(new Dimension(frameWidth, frameHeight));
        frame.add(canvas.getPanel(), BorderLayout.CENTER);
        frame.setLocation(x, y);
        frame.pack();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void showWindow() {
        frame.setVisible(true);
        canvas.getPanel().grabFocus();
    }

    @Override
    public Canvas getCanvas() {
        return canvas;
    }

    @Override
    public void fullScreenChanged() {
        if (isFullScreen()) {
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(frame);
        } else {
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(null);
        }
    }
}
