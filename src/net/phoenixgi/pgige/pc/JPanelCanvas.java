package net.phoenixgi.pgige.pc;

import net.phoenixgi.pgige.Canvas;
import net.phoenixgi.pgige.Scene;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class JPanelCanvas extends Canvas {

    JPanel panel;
    BufferedImage image;
    Graphics2D g2d;
    Scene currentScene;

    private int width;
    private int height;

    public JPanelCanvas(int width, int height) {
        this.width = width;
        this.height = height;
        buildPanel();
    }

    private void buildPanel() {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g2d = image.createGraphics();

        panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                int panelWidth = getWidth();
                int panelHeight = getHeight();

                g.setColor(Color.BLACK);
                g.fillRect(0, 0, panelWidth, panelHeight);

                float sy = (float) getHeight() / (float) height;
                float sx = (float) getWidth() / (float) width;
                float w = width, h = height;

                if (sx * h > getHeight()) {
                    w = sy * w;
                    h = sy * h;
                } else {
                    w = sx * w;
                    h = sx * h;
                }
                g.drawImage(image, (int) (panelWidth - w) / 2, (int) (panelHeight - h) / 2, (int) w, (int) h, null);
            }
        };
        InputAdapter input = new InputAdapter(this);
        panel.addKeyListener(input);
        panel.addMouseListener(input);
        panel.addMouseWheelListener(input);
        panel.addMouseMotionListener(input);
    }

    @Override
    public void render(Scene scene) {
        this.currentScene = scene;
        if (g2d != null) {
            g2d.setBackground(Color.WHITE);
            g2d.clearRect(0, 0, width, height);
            if (currentScene != null) {
                currentScene.render(g2d);
            }
        }
        panel.repaint();
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public JPanel getPanel() {
        return panel;
    }

    public StandardInput getInput() {
        return (StandardInput) currentScene.getInput();
    }

}
