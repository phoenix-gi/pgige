package net.phoenixgi.pgige;

import net.phoenixgi.pgige.object.GameObject;

import java.util.Vector;

/**
 * @author phoenix-gi
 */
public class SpatialGridHelper {

    public static boolean isOverlapY(float objectTopY, float objectBottomY, float gameObjectTopY, float gameObjectBottomY) {
        return objectTopY >= gameObjectTopY && objectTopY <= gameObjectBottomY
                || objectBottomY >= gameObjectTopY && objectBottomY <= gameObjectBottomY
                || gameObjectTopY >= objectTopY && gameObjectTopY <= objectBottomY
                || gameObjectBottomY >= objectTopY && gameObjectBottomY <= objectBottomY;
    }

    public static boolean isOverlapX(float objectLeftX, float objectRightX, float gameObjectLeftX, float gameObjectRightX) {
        return gameObjectLeftX >= objectLeftX && gameObjectLeftX <= objectRightX
                || gameObjectRightX >= objectLeftX && gameObjectRightX <= objectRightX
                || objectLeftX >= gameObjectLeftX && objectLeftX <= gameObjectRightX
                || objectRightX >= gameObjectLeftX && objectRightX <= gameObjectRightX;
    }

    public static GameObject getBottomSurfaceFor(GameObject gameObject, SpatialGrid grid) {
        float surfaceY = Float.MAX_VALUE;
        GameObject bottomSurface = null;

        float gameObjectLeftX = gameObject.getShape().getLeftX();
        float gameObjectTopY = gameObject.getShape().getTopY();
        float gameObjectRightX = gameObject.getShape().getRightX();
        float gameObjectBottomY = gameObject.getShape().getBottomY();

        Vector2 rightBottom = new Vector2(gameObjectRightX, gameObjectBottomY);
        Vector2 leftBottom = new Vector2(gameObjectLeftX, gameObjectTopY);
        int leftBottomId = grid.getCellIdForVector(leftBottom);
        int rightBottomId = grid.getCellIdForVector(rightBottom);

        while (rightBottomId > 0 && rightBottomId < grid.getCells().length) {
            Vector objectsRightBottom = grid.getObjectsInCell(rightBottomId);
            for (int i = 0; i < objectsRightBottom.size(); i++) {
                GameObject object = (GameObject) objectsRightBottom.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();

                if (isOverlapX(objectLeftX, objectRightX, gameObjectLeftX, gameObjectRightX)) {
                    if (objectTopY > gameObjectTopY && objectTopY < surfaceY) {
                        surfaceY = objectTopY;
                        bottomSurface = object;
                    }
                }
            }
            rightBottomId += grid.getCellsPerRow();
        }

        while (leftBottomId > 0 && leftBottomId < grid.getCells().length) {
            Vector objectsLeftBottom = grid.getObjectsInCell(leftBottomId);
            for (int i = 0; i < objectsLeftBottom.size(); i++) {
                GameObject object = (GameObject) objectsLeftBottom.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();
                if (isOverlapX(objectLeftX, objectRightX, gameObjectLeftX, gameObjectRightX)) {
                    if (objectTopY > gameObjectTopY && objectTopY < surfaceY) {
                        surfaceY = objectTopY;
                        bottomSurface = object;
                    }
                }
            }
            leftBottomId += grid.getCellsPerRow();
        }

        return bottomSurface;
    }

    public static GameObject getTopSurfaceYFor(GameObject gameObject, SpatialGrid grid) {
        float surfaceY = 0;
        GameObject topSurface = null;

        float gameObjectLeftX = gameObject.getShape().getLeftX();
        float gameObjectTopY = gameObject.getShape().getTopY();
        float gameObjectRightX = gameObject.getShape().getRightX();
        float gameObjectBottomY = gameObject.getShape().getBottomY();

        Vector2 rightBottom = new Vector2(gameObjectRightX, gameObjectBottomY);
        Vector2 leftBottom = new Vector2(gameObjectLeftX, gameObjectTopY);
        int leftBottomId = grid.getCellIdForVector(leftBottom);
        int rightBottomId = grid.getCellIdForVector(rightBottom);

        while (rightBottomId >= 0) {
            Vector objectsRightBottom = grid.getObjectsInCell(rightBottomId);
            for (int i = 0; i < objectsRightBottom.size(); i++) {
                GameObject object = (GameObject) objectsRightBottom.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();

                if (isOverlapX(objectLeftX, objectRightX, gameObjectLeftX, gameObjectRightX)) {
                    if (objectBottomY < gameObjectBottomY && objectBottomY > surfaceY) {
                        surfaceY = objectBottomY;
                        topSurface = object;
                    }
                }
            }
            rightBottomId -= grid.getCellsPerRow();
        }

        while (leftBottomId >= 0) {
            Vector objectsLeftBottom = grid.getObjectsInCell(leftBottomId);
            for (int i = 0; i < objectsLeftBottom.size(); i++) {
                GameObject object = (GameObject) objectsLeftBottom.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();
                if (isOverlapX(objectLeftX, objectRightX, gameObjectLeftX, gameObjectRightX)) {
                    if (objectBottomY < gameObjectBottomY && objectBottomY > surfaceY) {
                        surfaceY = objectBottomY;
                        topSurface = object;
                    }
                }
            }
            leftBottomId -= grid.getCellsPerRow();
        }

        return topSurface;
    }

    public static GameObject getLeftWall(GameObject gameObject, SpatialGrid grid) {
        float leftWallX = 0;
        GameObject leftWall = null;

        float gameObjectLeftX = gameObject.getShape().getLeftX();
        float gameObjectTopY = gameObject.getShape().getTopY();
        float gameObjectRightX = gameObject.getShape().getRightX();
        float gameObjectBottomY = gameObject.getShape().getBottomY();
        float gameObjectCenterX = (gameObjectLeftX + gameObjectRightX) / 2;
        float gameObjectCenterY = (gameObjectTopY + gameObjectBottomY) / 2;

        Vector2 leftTop = new Vector2(gameObjectLeftX, gameObjectTopY);
        Vector2 leftBottom = new Vector2(gameObjectLeftX, gameObjectBottomY);
        int leftTopCellId = grid.getCellIdForVector(leftTop);
        int leftBottomCellId = grid.getCellIdForVector(leftBottom);

        int rowTop = leftTopCellId / grid.getCellsPerRow();
        while (leftTopCellId >= 0 && leftTopCellId / grid.getCellsPerRow() == rowTop) {
            Vector objectsTopLeft = grid.getObjectsInCell(leftTopCellId);
            for (int i = 0; i < objectsTopLeft.size(); i++) {
                GameObject object = (GameObject) objectsTopLeft.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();

                if (isOverlapY(objectTopY, objectBottomY, gameObjectTopY, gameObjectBottomY)) {
                    if (gameObjectRightX > objectRightX && objectRightX > leftWallX) {
                        leftWallX = objectRightX;
                        leftWall = object;
                    }
                }
            }
            leftTopCellId--;
        }

        int rowBottom = leftBottomCellId / grid.getCellsPerRow();
        while (leftBottomCellId >= 0 && leftBottomCellId / grid.getCellsPerRow() == rowBottom) {
            Vector objectsBottomLeft = grid.getObjectsInCell(leftBottomCellId);
            for (int i = 0; i < objectsBottomLeft.size(); i++) {
                GameObject object = (GameObject) objectsBottomLeft.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();

                if (isOverlapY(objectTopY, objectBottomY, gameObjectTopY, gameObjectBottomY)) {
                    if (gameObjectRightX > objectRightX && objectRightX > leftWallX) {
                        leftWallX = objectRightX;
                        leftWall = object;
                    }
                }
            }
            leftBottomCellId--;
        }

        return leftWall;
    }

    public static GameObject getRightWall(GameObject gameObject, SpatialGrid grid) {
        float rightWallX = Float.MAX_VALUE;
        GameObject rightWall = null;

        float gameObjectLeftX = gameObject.getShape().getLeftX();
        float gameObjectTopY = gameObject.getShape().getTopY();

        float gameObjectRightX = gameObject.getShape().getRightX();
        float gameObjectBottomY = gameObject.getShape().getBottomY();

        Vector2 rightTop = new Vector2(gameObjectRightX, gameObjectTopY);
        Vector2 rightBottom = new Vector2(gameObjectRightX, gameObjectBottomY);

        int rightTopCellId = grid.getCellIdForVector(rightTop);
        int rightBottomCellId = grid.getCellIdForVector(rightBottom);

        int rowTop = rightTopCellId / grid.getCellsPerRow();
        while (rightTopCellId / grid.getCellsPerRow() == rowTop) {
            Vector objectsTopRight = grid.getObjectsInCell(rightTopCellId);
            for (int i = 0; i < objectsTopRight.size(); i++) {
                GameObject object = (GameObject) objectsTopRight.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();

                if (isOverlapY(objectTopY, objectBottomY, gameObjectTopY, gameObjectBottomY)) {
                    if (gameObjectLeftX < objectLeftX && objectLeftX < rightWallX) {
                        rightWallX = objectLeftX;
                        rightWall = object;
                    }
                }
            }
            rightTopCellId++;
        }

        int rowBottom = rightBottomCellId / grid.getCellsPerRow();
        while (rightBottomCellId / grid.getCellsPerRow() == rowBottom) {
            Vector objectsBottomRight = grid.getObjectsInCell(rightBottomCellId);
            for (int i = 0; i < objectsBottomRight.size(); i++) {
                GameObject object = (GameObject) objectsBottomRight.elementAt(i);
                float objectTopY = object.getShape().getTopY();
                float objectLeftX = object.getShape().getLeftX();
                float objectRightX = object.getShape().getRightX();
                float objectBottomY = object.getShape().getBottomY();

                if (isOverlapY(objectTopY, objectBottomY, gameObjectTopY, gameObjectBottomY)) {
                    if (gameObjectLeftX < objectLeftX && objectLeftX < rightWallX) {
                        rightWallX = objectLeftX;
                        rightWall = object;
                    }
                }
            }
            rightBottomCellId++;
        }

        return rightWall;
    }
}

