package net.phoenixgi.pgige;

public class SceneManager {
    private Scene scene;

    public void setScene(Scene scene) {
        if (this.scene != null) {
            this.scene.destroy();
        }
        this.scene = scene;
        this.scene.initialize();
    }

    public Scene getCurrentScene() {
        return scene;
    }
}
