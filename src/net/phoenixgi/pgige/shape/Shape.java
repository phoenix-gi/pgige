package net.phoenixgi.pgige.shape;

import net.phoenixgi.pgige.Vector2;

/**
 * @author phoenix-gi
 */
public abstract class Shape {

    Vector2 center;

    public Shape() {
        center = new Vector2(0, 0);
    }

    public Vector2 getCenter() {
        return center;
    }

    public void setCenter(Vector2 center) {
        this.center.set(center);
    }

    public abstract void rotate(float angle);

    public abstract float getLeftX();

    public abstract float getRightX();

    public abstract float getTopY();

    public abstract float getBottomY();

    public float getWidth() {
        return getRightX() - getLeftX();
    }

    public float getHeight() {
        return getBottomY() - getTopY();
    }
}
