package net.phoenixgi.pgige.shape;

import net.phoenixgi.pgige.Vector2;

/**
 * @author phoenix-gi
 */
public final class Rectangle extends Shape {

    private final Vector2 p1;
    private final Vector2 p2;
    private final Vector2 p3;
    private final Vector2 p4;

    public Rectangle(float width, float height) {
        super();
        /*
        p1             width            p2
          *----------------------------*
          |                            |he
          |            .center         |  ig
          |                            |    ht
          *----------------------------*
        p4                              p3
         */
        p1 = new Vector2(center.x - width / 2, center.y - height / 2);
        p2 = new Vector2(center.x + width / 2, center.y - height / 2);
        p3 = new Vector2(center.x + width / 2, center.y + height / 2);
        p4 = new Vector2(center.x - width / 2, center.y + height / 2);
    }

    public Vector2 getFirstPoint() {
        return p1.clone();
    }

    public Vector2 getSecondPoint() {
        return p2.clone();
    }

    public Vector2 getThirdPoint() {
        return p3.clone();
    }

    public Vector2 getFourthPoint() {
        return p4.clone();
    }

    public Line[] getLines() {
        return new Line[]{new Line(p1.x, p1.y, p2.x, p2.y), new Line(p2.x, p2.y, p3.x, p3.y), new Line(p3.x, p3.y, p4.x, p4.y), new Line(p4.x, p4.y, p1.x, p1.y)};
    }

    public void move(float x, float y) {
        p1.add(x, y);
        p2.add(x, y);
        p3.add(x, y);
        p4.add(x, y);
        center.add(x, y);
    }

    @Override
    public void rotate(float angle) {
        float cx = center.x;
        float cy = center.y;
        move(-cx, -cy);
        p1.rotate(angle);
        p2.rotate(angle);
        p3.rotate(angle);
        p4.rotate(angle);
        move(cx, cy);
    }

    public Vector2[] getPoints() {
        return new Vector2[]{p1.clone(), p2.clone(), p3.clone(), p4.clone()};
    }

    @Override
    public void setCenter(Vector2 c) {
        move(-center.x, -center.y);
        move(c.x, c.y);
    }

    @Override
    public float getLeftX() {
        float minX = p1.x;
        for (Vector2 point : getPoints()) {
            if (point.x < minX) {
                minX = point.x;
            }
        }
        return minX;
    }

    @Override
    public float getRightX() {
        float maxX = p1.x;
        for (Vector2 point : getPoints()) {
            if (point.x > maxX) {
                maxX = point.x;
            }
        }
        return maxX;
    }

    @Override
    public float getTopY() {
        float minY = p1.y;
        for (Vector2 point : getPoints()) {
            if (point.y < minY) {
                minY = point.y;
            }
        }
        return minY;
    }

    @Override
    public float getBottomY() {
        float maxY = p1.y;
        for (Vector2 point : getPoints()) {
            if (point.y > maxY) {
                maxY = point.y;
            }
        }
        return maxY;
    }
}
