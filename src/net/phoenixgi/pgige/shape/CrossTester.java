package net.phoenixgi.pgige.shape;

import net.phoenixgi.pgige.Vector2;

/**
 * @author phoenix-gi
 */
public class CrossTester {

    public static boolean isCross(Shape s1, Shape s2) {
        if (s1.getClass() == Triangle.class && s2.getClass() == Triangle.class) {
            return isCross((Triangle) s1, (Triangle) s2);
        } else if (s1.getClass() == Triangle.class && s2.getClass() == Rectangle.class) {
            return isCross((Triangle) s1, (Rectangle) s2);
        } else if (s1.getClass() == Rectangle.class && s2.getClass() == Triangle.class) {
            return isCross((Triangle) s2, (Rectangle) s1);
        } else if (s1.getClass() == Rectangle.class && s2.getClass() == Rectangle.class) {
            return isCross((Rectangle) s1, (Rectangle) s2);
        } else if (s1.getClass() == Line.class && s2.getClass() == Line.class) {
            return isCross((Line) s1, (Line) s2);
        } else if (s1.getClass() == Line.class && s2.getClass() == Circle.class) {
            return isCross((Line) s1, (Circle) s2);
        } else if (s1.getClass() == Circle.class && s2.getClass() == Line.class) {
            return isCross((Line) s2, (Circle) s1);
        } else if (s1.getClass() == Circle.class && s2.getClass() == Circle.class) {
            return isCross((Circle) s2, (Circle) s1);
        } else if (s1.getClass() == Circle.class && s2.getClass() == Rectangle.class) {
            return isCross((Circle) s1, (Rectangle) s2);
        } else if (s2.getClass() == Circle.class && s1.getClass() == Rectangle.class) {
            return isCross((Circle) s2, (Rectangle) s1);
        } else if (s2.getClass() == Line.class && s1.getClass() == Rectangle.class) {
            return isCross((Line) s2, (Rectangle) s1);
        } else if (s1.getClass() == Line.class && s2.getClass() == Rectangle.class) {
            return isCross((Line) s1, (Rectangle) s2);
        } else if (s2.getClass() == Line.class && s1.getClass() == Triangle.class) {
            return isCross((Line) s2, (Triangle) s1);
        } else if (s1.getClass() == Line.class && s2.getClass() == Triangle.class) {
            return isCross((Line) s1, (Triangle) s2);
        } else if (s2.getClass() == Circle.class && s1.getClass() == Triangle.class) {
            return isCross((Circle) s2, (Triangle) s1);
        } else if (s1.getClass() == Circle.class && s2.getClass() == Triangle.class) {
            return isCross((Circle) s1, (Triangle) s2);
        }
        return false;
    }

    public static boolean isCross(Circle c1, Circle c2) {
        return c1.getCenter().distSquared(c2.getCenter()) <= (c1.getRadius() + c2.getRadius()) * (c1.getRadius() + c2.getRadius());
    }

    public static boolean isCross(Circle c, Rectangle r) {
        Line[] lines = r.getLines();
        for (Line line : lines) {
            if (isCross(line, c)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCross(Circle c, Triangle t) {
        Line[] lines = t.getLines();
        for (Line line : lines) {
            if (isCross(line, c)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCross(Line l, Triangle t) {
        Line[] lines = t.getLines();
        for (Line line : lines) {
            if (isCross(line, l)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCross(Line l, Rectangle r) {
        Line[] lines = r.getLines();
        for (Line line : lines) {
            if (isCross(line, l)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCross(Triangle t1, Triangle t2) {
        for (Vector2 p : t2.getPoints()) {
            if (isCross(t1, p)) {
                return true;
            }
        }
        for (Line line1 : t1.getLines()) {
            for (Line line2 : t2.getLines()) {
                if (isCross(line1, line2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isCross(Triangle t1, Vector2 p) {
        Vector2[] p1 = t1.getPoints();
        float x1, x2, x3, x4, y1, y2, y3, y4;
        x1 = p1[0].x - p1[0].x;
        y1 = p1[0].y - p1[0].y;
        x2 = p1[1].x - p1[0].x;
        y2 = p1[1].y - p1[0].y;
        x3 = p1[2].x - p1[0].x;
        y3 = p1[2].y - p1[0].y;
        x4 = p.x - p1[0].x;
        y4 = p.y - p1[0].y;

        float dy21 = y2 - y1;
        float dx31 = x3 - x1;
        float dy31 = y3 - y1;

        float m, n, k, l;
        k = (float) (dx31 / Math.sqrt(dx31 * dx31 + dy31 * dy31));
        l = (float) (dy31 / Math.sqrt(dx31 * dx31 + dy31 * dy31));
        if (dx31 < 1E-5 && dx31 > -1E-5) {
            n = 0;
            m = 1;
        } else {
            float cos = (float) Math.cos(-Math.PI / 2);
            float sin = (float) Math.sin(-Math.PI / 2);
            m = k * cos - l * sin;
            n = k * sin + l * cos;
        }

        float b1 = (y1 * k - x1 * l) / (k * n - m * l);
        float a1 = (-y1 * m + x1 * n) / (k * n - m * l);

        float b3 = (y3 * k - x3 * l) / (k * n - m * l);
        float a3 = (-y3 * m + x3 * n) / (k * n - m * l);

        float b2 = (y2 * k - x2 * l) / (k * n - m * l);
        float a2 = (-y2 * m + x2 * n) / (k * n - m * l);

        float b4 = (y4 * k - x4 * l) / (k * n - m * l);
        float a4 = (-y4 * m + x4 * n) / (k * n - m * l);
        if (a2 > a1 && a2 < a3) {
            if (b4 <= ((b1 - b2) / (a1 - a2) * a4 + (a1 * b2 - a2 * b1) / (a1 - a2))) {
                if (b4 <= ((b3 - b2) / (a3 - a2) * a4 + (a3 * b2 - a2 * b3) / (a3 - a2))) {
                    if (b4 > b1) {
                        return true;
                    }
                }
            }
        } else if (a2 > a3) {
            if (b4 <= ((b1 - b2) / (a1 - a2) * a4 + (a1 * b2 - a2 * b1) / (a1 - a2))) {
                if (b4 >= ((b3 - b2) / (a3 - a2) * a4 + (a3 * b2 - a2 * b3) / (a3 - a2))) {
                    if (b4 > b1) {
                        return true;
                    }
                }
            }
        } else if (a2 < a1) {
            if (b4 >= ((b1 - b2) / (a1 - a2) * a4 + (a1 * b2 - a2 * b1) / (a1 - a2))) {
                if (b4 <= ((b3 - b2) / (a3 - a2) * a4 + (a3 * b2 - a2 * b3) / (a3 - a2))) {
                    if (b4 > b1) {
                        return true;
                    }
                }
            }
        } else if (a2 == a1) {
            if (a4 >= a1) {
                if (b4 <= ((b3 - b2) / (a3 - a2) * a4 + (a3 * b2 - a2 * b3) / (a3 - a2))) {
                    if (b4 > b1) {
                        return true;
                    }
                }
            }
        } else if (a2 == a3) {
            if (b4 <= ((b1 - b2) / (a1 - a2) * a4 + (a1 * b2 - a2 * b1) / (a1 - a2))) {
                if (a4 <= a3) {
                    if (b4 > b1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isCross(Triangle t, Rectangle r) {
        for (Vector2 p : r.getPoints()) {
            if (isCross(t, p)) {
                return true;
            }
        }
        for (Vector2 p : t.getPoints()) {
            if (isCross(r, p)) {
                return true;
            }
        }
        for (Line line1 : t.getLines()) {
            for (Line line2 : r.getLines()) {
                if (isCross(line1, line2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isCross(Rectangle r, Vector2 p) {
        float x1, x2, x3, x4, x5, y1, y2, y3, y4, y5;
        x1 = r.getFirstPoint().x;
        y1 = r.getFirstPoint().y;

        x2 = r.getSecondPoint().x;
        y2 = r.getSecondPoint().y;

        x3 = r.getThirdPoint().x;
        y3 = r.getThirdPoint().y;

        x4 = r.getFourthPoint().x;
        y4 = r.getFourthPoint().y;

        x5 = p.x;
        y5 = p.y;

        float k, l, m, n;
        float dx21, dx41, dy21, dy41;
        dx21 = x2 - x1;
        dy21 = y2 - y1;
        dx41 = x4 - x1;
        dy41 = y4 - y1;
        k = (float) (dx21 / Math.sqrt(dx21 * dx21 + dy21 * dy21));
        l = (float) (dy21 / Math.sqrt(dx21 * dx21 + dy21 * dy21));
        m = (float) (dx41 / Math.sqrt(dx41 * dx41 + dy41 * dy41));
        n = (float) (dy41 / Math.sqrt(dx41 * dx41 + dy41 * dy41));

        float a1, a2, a3, a4, a5, b1, b2, b3, b4, b5;
        a1 = (-y1 * m + x1 * n) / (k * n - m * l);
        b1 = (y1 * k - x1 * l) / (k * n - m * l);

        a2 = (-y2 * m + x2 * n) / (k * n - m * l);
        b2 = (y2 * k - x2 * l) / (k * n - m * l);

        a3 = (-y3 * m + x3 * n) / (k * n - m * l);
        b3 = (y3 * k - x3 * l) / (k * n - m * l);

        a4 = (-y4 * m + x4 * n) / (k * n - m * l);
        b4 = (y4 * k - x4 * l) / (k * n - m * l);

        a5 = (-y5 * m + x5 * n) / (k * n - m * l);
        b5 = (y5 * k - x5 * l) / (k * n - m * l);

        return b5 >= b1 && b5 <= b4 && a5 >= a1 && a5 <= a2;
    }

    public static boolean isCross(Rectangle r1, Rectangle r2) {
        Vector2[] v = r1.getPoints();
        for (Vector2 v2 : v) {
            if (isCross(r2, v2)) {
                return true;
            }
        }

        v = r2.getPoints();
        for (Vector2 v1 : v) {
            if (isCross(r1, v1)) {
                return true;
            }
        }
        for (Line line1 : r1.getLines()) {
            for (Line line2 : r2.getLines()) {
                if (isCross(line1, line2)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean isCross(Line l1, Line l2) {
        float x1 = l1.getFirstPoint().x;
        float y1 = l1.getFirstPoint().y;
        float x2 = l1.getSecondPoint().x;
        float y2 = l1.getSecondPoint().y;

        float x3 = l2.getFirstPoint().x;
        float y3 = l2.getFirstPoint().y;
        float x4 = l2.getSecondPoint().x;
        float y4 = l2.getSecondPoint().y;

        float b1, b2, c1, c2;

        float x;
        float y;

        if (x2 - x1 == 0 && x3 - x4 != 0) {
            b2 = (y4 - y3) / (x4 - x3);
            c2 = (x3 * y4 - x4 * y3) / (x3 - x4);
            x = x1;
            y = b2 * x + c2;
        } else if (x2 - x1 != 0 && x3 - x4 == 0) {
            b1 = (y2 - y1) / (x2 - x1);
            c1 = (x1 * y2 - x2 * y1) / (x1 - x2);
            x = x3;
            y = b1 * x + c1;
        } else if (x2 - x1 == 0 && x3 - x4 == 0) {
            return x2 == x3 && (y3 >= Math.min(y1, y2) && y3 <= Math.max(y1, y2) || y4 >= Math.min(y1, y2) && y4 <= Math.max(y1, y2));
        } else {
            b1 = (y2 - y1) / (x2 - x1);
            c1 = (x1 * y2 - x2 * y1) / (x1 - x2);
            b2 = (y4 - y3) / (x4 - x3);
            c2 = (x3 * y4 - x4 * y3) / (x3 - x4);
            if (b1 == b2) {
                return c1 == c2 && (x1 >= Math.min(x3, x4) && x1 <= Math.max(x3, x4) && y1 >= Math.min(y3, y4) && y1 <= Math.max(y3, y4)
                        || x2 >= Math.min(x3, x4) && x2 <= Math.max(x3, x4) && y2 >= Math.min(y3, y4) && y2 <= Math.max(y3, y4)
                        || x3 >= Math.min(x1, x2) && x3 <= Math.max(x1, x2) && y3 >= Math.min(y1, y2) && y3 <= Math.max(y1, y2)
                        || x4 >= Math.min(x1, x2) && x4 <= Math.max(x1, x2) && y4 >= Math.min(y1, y2) && y4 <= Math.max(y1, y2));
            }
            x = (c2 - c1) / (b1 - b2);
            y = (b1 * c2 - b2 * c1) / (b1 - b2);
        }
        return x >= Math.min(x1, x2) && x <= Math.max(x1, x2) && y >= Math.min(y1, y2) && y <= Math.max(y1, y2) && x >= Math.min(x3, x4) && x <= Math.max(x3, x4) && y >= Math.min(y3, y4) && y <= Math.max(y3, y4);
    }

    public static boolean isPointNearLine(Vector2 p, Line l) {
        float x1 = l.getFirstPoint().x;
        float y1 = l.getFirstPoint().y;
        float x2 = l.getSecondPoint().x;
        float y2 = l.getSecondPoint().y;
        if (x1 - x2 != 0) {
            float b1 = (y2 - y1) / (x2 - x1);
            float c1 = (x1 * y2 - x2 * y1) / (x1 - x2);
            float d = p.y - p.x * b1 - c1;
            if (d <= 1f && d >= -1f) {
                return true;
            }
        } else {
            float t = y1;
            y1 = Math.max(y1, y2);
            y2 = Math.min(t, y2);
            if (p.y <= y1 && p.y >= y2 && p.x >= x1 - 1 && p.x <= x1 + 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCross(Line li, Circle c) {
        float x1 = li.getFirstPoint().x - li.getFirstPoint().x;
        float y1 = li.getFirstPoint().y - li.getFirstPoint().y;
        float x2 = li.getSecondPoint().x - li.getFirstPoint().x;
        float y2 = li.getSecondPoint().y - li.getFirstPoint().y;
        float x3 = c.getCenter().x - li.getFirstPoint().x;
        float y3 = c.getCenter().y - li.getFirstPoint().y;

        float dx21 = x2 - x1;
        float dy21 = y2 - y1;

        float m, n, k, l;
        k = (float) (dx21 / Math.sqrt(dx21 * dx21 + dy21 * dy21));
        l = (float) (dy21 / Math.sqrt(dx21 * dx21 + dy21 * dy21));

        float cos = (float) Math.cos(-Math.PI / 2);
        float sin = (float) Math.sin(-Math.PI / 2);
        m = k * cos - l * sin;
        n = k * sin + l * cos;

        float b1 = (y1 * k - x1 * l) / (k * n - m * l);
        float a1 = (-y1 * m + x1 * n) / (k * n - m * l);
        float b2 = (y2 * k - x2 * l) / (k * n - m * l);
        float a2 = (-y2 * m + x2 * n) / (k * n - m * l);
        float b3 = (y3 * k - x3 * l) / (k * n - m * l);
        float a3 = (-y3 * m + x3 * n) / (k * n - m * l);

        boolean A = c.getRadius() >= Math.abs(b3);
        boolean B = a3 >= 0 && a3 <= a2;
        boolean C = ((a3 - a1) * (a3 - a1) + (b3 - b1) * (b3 - b1)) <= c.getRadius() * c.getRadius() || ((a3 - a2) * (a3 - a2) + (b3 - b2) * (b3 - b2)) <= c.getRadius() * c.getRadius();
        return A && B || C;
    }

}
