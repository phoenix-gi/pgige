package net.phoenixgi.pgige.shape;

import net.phoenixgi.pgige.Vector2;

/**
 * @author phoenix-gi
 */
public final class Line extends Shape {

    Vector2 p1;
    Vector2 p2;

    public Line(float x1, float y1, float x2, float y2) {
        p1 = new Vector2(x1, y1);
        p2 = new Vector2(x2, y2);
        center.set(new Vector2((x1 + x2) / 2, (y1 + y2) / 2));
    }

    public Line(float length) {
        p1 = new Vector2(-length / 2, 0);
        p2 = new Vector2(length / 2, 0);
    }

    public Vector2 getFirstPoint() {
        return p1;
    }

    public Vector2 getSecondPoint() {
        return p2;
    }


    public Vector2[] getPoints() {
        return new Vector2[]{p1.clone(), p2.clone()};
    }


    public void move(float x, float y) {
        p1.add(x, y);
        p2.add(x, y);
        center.add(x, y);
    }

    @Override
    public void setCenter(Vector2 c) {
        move(-center.x, -center.y);
        move(c.x, c.y);
    }

    @Override
    public void rotate(float angle) {
        float cx = center.x;
        float cy = center.y;
        move(-cx, -cy);
        p1.rotate(angle);
        p2.rotate(angle);
        move(cx, cy);
    }


    @Override
    public float getLeftX() {
        float minX = p1.x;
        for (Vector2 point : getPoints()) {
            if (point.x < minX) {
                minX = point.x;
            }
        }
        return minX;
    }

    @Override
    public float getRightX() {
        float maxX = p1.x;
        for (Vector2 point : getPoints()) {
            if (point.x > maxX) {
                maxX = point.x;
            }
        }
        return maxX;
    }

    @Override
    public float getTopY() {
        float minY = p1.y;
        for (Vector2 point : getPoints()) {
            if (point.y < minY) {
                minY = point.y;
            }
        }
        return minY;
    }

    @Override
    public float getBottomY() {
        float maxY = p1.y;
        for (Vector2 point : getPoints()) {
            if (point.y > maxY) {
                maxY = point.y;
            }
        }
        return maxY;
    }
}
