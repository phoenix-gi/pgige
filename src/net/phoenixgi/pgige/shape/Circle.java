package net.phoenixgi.pgige.shape;

/**
 * @author phoenix-gi
 */
public class Circle extends Shape {

    float radius;

    public Circle(float radius) {
        super();
        this.radius = radius;
    }

    public Circle() {
        super();
        radius = 1;
    }

    @Override
    public void rotate(float angle) {

    }

    @Override
    public float getLeftX() {
        return getCenter().x - radius;
    }

    @Override
    public float getRightX() {
        return getCenter().x + radius;
    }

    @Override
    public float getTopY() {
        return getCenter().y - radius;
    }

    @Override
    public float getBottomY() {
        return getCenter().y + radius;
    }

    @Override
    public float getWidth() {
        return radius * 2;
    }

    @Override
    public float getHeight() {
        return radius * 2;
    }

    public float getRadius() {
        return radius;
    }
}
