package net.phoenixgi.pgige;

public class GameUpdater {
    private Game game;

    public GameUpdater(Game game) {
        this.game = game;
    }

    public void start() {
        long startTime = System.nanoTime();
        float deltaTime = 0;
        while (true) {
            try {
                Thread.sleep(10L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Scene scene = game.getSceneManager().getCurrentScene();
            if (scene != null) {
                scene.update(deltaTime);
                game.getCanvas().render(scene);
            }
            long currentTime = System.nanoTime();
            deltaTime = (currentTime - startTime) / 1_000_000_000.0f;
            startTime = currentTime;
        }
    }
}
