package net.phoenixgi.pgige;

import java.io.IOException;
import javax.sound.sampled.*;

/**
 * @author phoenix-gi
 */
public class Audio {
    private Clip clip;

    public Audio(String fileName) {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(Audio.class.getResource(fileName));
            DataLine.Info info = new DataLine.Info(Clip.class, ais.getFormat());
            for (Mixer.Info mixerInfo : AudioSystem.getMixerInfo()) {
                try {
                    Mixer mixer = AudioSystem.getMixer(mixerInfo);
                    clip = (Clip) mixer.getLine(info);
                    if (clip == null) {
                        continue; //Doesn't support this format
                    }
                    clip.open(ais);
                    break;
                } catch (Exception ex) {
                    //If we get here it a buggered line, so loop round again
                    continue;
                }
            }
        } catch (UnsupportedAudioFileException | IOException ex) {
            ex.printStackTrace();
        }
    }

    public void play() {
        clip.setFramePosition(0);
        clip.start();
    }

    public boolean isRunning() {
        return clip.isRunning();
    }

    public void loop() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop() {
        clip.stop();
    }

    public void dispose() {
        clip.close();
    }
}
