package net.phoenixgi.pgige.object;

import net.phoenixgi.pgige.shape.Line;

public class LineObject extends GameObject {
    public LineObject(float width) {
        Line line = new Line(width);
        setShape(line);
    }

    @Override
    public Line getShape() {
        return (Line) super.getShape();
    }
}