package net.phoenixgi.pgige.object;

import net.phoenixgi.pgige.shape.Rectangle;

public class RectangleObject extends GameObject {
    public RectangleObject(float width, float height) {
        Rectangle rectangle = new Rectangle(width, height);
        setShape(rectangle);
    }

    @Override
    public Rectangle getShape() {
        return (Rectangle) super.getShape();
    }
}
