package net.phoenixgi.pgige.object;

import net.phoenixgi.pgige.shape.Circle;

public class CircleObject extends GameObject {
    public CircleObject(float radius) {
        Circle circle = new Circle(radius);
        setShape(circle);
    }

    @Override
    public Circle getShape() {
        return (Circle) super.getShape();
    }
}
