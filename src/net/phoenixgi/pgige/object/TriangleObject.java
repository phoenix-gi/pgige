package net.phoenixgi.pgige.object;

import net.phoenixgi.pgige.Vector2;
import net.phoenixgi.pgige.shape.Triangle;

public class TriangleObject extends GameObject {
    public TriangleObject(Vector2 p1, Vector2 p2, Vector2 p3) {
        Triangle triangle = new Triangle(p1, p2, p3);
        setShape(triangle);
    }

    @Override
    public Triangle getShape() {
        return (Triangle) super.getShape();
    }
}
