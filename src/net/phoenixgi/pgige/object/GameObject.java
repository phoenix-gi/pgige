package net.phoenixgi.pgige.object;

import net.phoenixgi.pgige.ObjectController;
import net.phoenixgi.pgige.shape.Shape;
import net.phoenixgi.pgige.view.ObjectView;

/**
 * @author phoenix-gi
 */
public abstract class GameObject {

    protected Shape shape;
    private ObjectView view;
    private ObjectController controller;

    public GameObject() {
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Shape getShape() {
        return shape;
    }

    public ObjectView getView() {
        return view;
    }

    public void setView(ObjectView view) {
        this.view = view;
    }

    public void setController(ObjectController controller) {
        this.controller = controller;
    }

    public ObjectController getController() {
        return controller;
    }
}
