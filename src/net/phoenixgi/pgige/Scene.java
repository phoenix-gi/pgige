package net.phoenixgi.pgige;

public abstract class Scene {

    private final SceneManager sceneManager;
    private final Game game;
    private Input input;

    public Scene(SceneManager sceneManager, Game game) {
        this.sceneManager = sceneManager;
        this.game = game;
    }

    public SceneManager getSceneManager() {
        return sceneManager;
    }

    public Game getGame() {
        return game;
    }

    public Canvas getCanvas() {
        return game.getCanvas();
    }

    public int getWidth() {
        return game.getCanvas().getWidth();
    }

    public int getHeight() {
        return game.getCanvas().getHeight();
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public abstract void initialize();

    public abstract void update(float deltaTime);

    public abstract void render(Object drawingContext);

    public abstract void destroy();
}
