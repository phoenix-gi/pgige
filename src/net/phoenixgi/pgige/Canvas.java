package net.phoenixgi.pgige;

public abstract class Canvas {
    public abstract void render(Scene scene);

    public abstract int getWidth();

    public abstract int getHeight();
}
