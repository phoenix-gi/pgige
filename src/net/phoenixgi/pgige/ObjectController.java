package net.phoenixgi.pgige;

import net.phoenixgi.pgige.object.GameObject;

/**
 * @author phoenix-gi
 */
public abstract class ObjectController {

    protected GameObject gameObject;

    public ObjectController(GameObject gameObject) {
        this.gameObject = gameObject;
    }

    public abstract void update(float deltaTime);

    public GameObject getGameObject() {
        return gameObject;
    }
}
