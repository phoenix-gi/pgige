package net.phoenixgi.pgige;

import java.awt.*;

/**
 * @author phoenix-gi
 */
public class Sprite2D {

    private Image image;
    private int frameWidth;
    private int frameHeight;
    private int frameIndex;

    public Sprite2D(Image image, int frameWidth, int frameHeight) {
        this.image = image;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.frameIndex = 0;
    }

    public Image getImage() {
        return image;
    }

    public int getFrameWidth() {
        return frameWidth;
    }

    public int getFrameHeight() {
        return frameHeight;
    }

    public void setFrameIndex(int frameIndex) {
        this.frameIndex = frameIndex;
    }

    public int getFrameIndex() {
        return frameIndex;
    }

    public void paint(Graphics g, int x, int y, int transform) {
        int xSrc = frameIndex * frameWidth;
        int ySrc = 0;
        if (transform == 0) {
            g.drawImage(image, x, y, x + frameWidth, y + frameHeight, xSrc, ySrc, xSrc + frameWidth, ySrc + frameHeight, null);
        } else if (transform == 1) {
            g.drawImage(image, x + frameWidth, y, x, y + frameHeight, xSrc, ySrc, xSrc + frameWidth, ySrc + frameHeight, null);
        }
//        g.drawRegion(image, xSrc, ySrc, frameWidth, frameHeight, transform, x, y, 0);
    }
}
