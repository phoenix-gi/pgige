package net.phoenixgi.pgige.view;

import net.phoenixgi.pgige.object.GameObject;
import net.phoenixgi.pgige.shape.Line;
import net.phoenixgi.pgige.shape.Rectangle;

import java.awt.*;

/**
 * @author phoenix-gi
 */
public class RectangleView extends DebugView {
    public RectangleView(GameObject gameObject) {
        super(gameObject, 0x0000FF);
    }

    public RectangleView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    @Override
    public void render(Graphics g) {
        Rectangle r = (Rectangle) gameObject.getShape();
        g.setColor(new Color(color));
        g.drawLine((int) r.getFirstPoint().x, (int) r.getFirstPoint().y, (int) r.getSecondPoint().x, (int) r.getSecondPoint().y);
        g.drawLine((int) r.getFirstPoint().x, (int) r.getFirstPoint().y, (int) r.getFourthPoint().x, (int) r.getFourthPoint().y);
        g.drawLine((int) r.getThirdPoint().x, (int) r.getThirdPoint().y, (int) r.getFourthPoint().x, (int) r.getFourthPoint().y);
        g.drawLine((int) r.getThirdPoint().x, (int) r.getThirdPoint().y, (int) r.getSecondPoint().x, (int) r.getSecondPoint().y);

        for (Line l : r.getLines()) {
            g.drawLine((int) l.getFirstPoint().x, (int) l.getFirstPoint().y, (int) l.getSecondPoint().x, (int) l.getSecondPoint().y);
        }
    }
}
