package net.phoenixgi.pgige.view;


import net.phoenixgi.pgige.object.GameObject;

import java.awt.*;

/**
 *
 * @author phoenix-gi
 */
public abstract class ObjectView {
    protected GameObject gameObject;
    
    public ObjectView(GameObject gameObject) {
        this.gameObject = gameObject;
        gameObject.setView(this);
    }
    
    public abstract void render(Graphics g);

    public GameObject getGameObject() {
        return gameObject;
    }
}
