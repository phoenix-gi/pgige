package net.phoenixgi.pgige.view;

import net.phoenixgi.pgige.object.GameObject;
import net.phoenixgi.pgige.shape.Line;

import java.awt.*;

/**
 * @author phoenix-gi
 */
public class LineView extends DebugView {

    public LineView(GameObject gameObject) {
        super(gameObject, 0x0000FF);
    }

    public LineView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    @Override
    public void render(Graphics g) {
        Line l = (Line) gameObject.getShape();
        g.setColor(new Color(color));
        g.drawLine((int) l.getFirstPoint().x, (int) l.getFirstPoint().y, (int) l.getSecondPoint().x, (int) l.getSecondPoint().y);
    }
}
