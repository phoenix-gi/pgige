package net.phoenixgi.pgige.view;

import net.phoenixgi.pgige.object.GameObject;
import net.phoenixgi.pgige.shape.Line;
import net.phoenixgi.pgige.shape.Triangle;

import java.awt.*;

/**
 * @author phoenix-gi
 */
public class TriangleView extends DebugView {

    public TriangleView(GameObject gameObject) {
        super(gameObject, 0x0000FF);
    }

    public TriangleView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    @Override
    public void render(Graphics g) {
        Triangle t = (Triangle) gameObject.getShape();

        g.setColor(new Color(color));
        g.drawLine((int) t.getFirstPoint().x, (int) t.getFirstPoint().y, (int) t.getSecondPoint().x, (int) t.getSecondPoint().y);
        g.drawLine((int) t.getFirstPoint().x, (int) t.getFirstPoint().y, (int) t.getThirdPoint().x, (int) t.getThirdPoint().y);
        g.drawLine((int) t.getThirdPoint().x, (int) t.getThirdPoint().y, (int) t.getSecondPoint().x, (int) t.getSecondPoint().y);

        for (Line l : t.getLines()) {
            g.drawLine((int) l.getFirstPoint().x, (int) l.getFirstPoint().y, (int) l.getSecondPoint().x, (int) l.getSecondPoint().y);
        }
    }

}
