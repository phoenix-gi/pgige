package net.phoenixgi.pgige.view;

import net.phoenixgi.pgige.object.GameObject;
import net.phoenixgi.pgige.shape.Circle;

import java.awt.*;

/**
 * @author phoenix-gi
 */
public class CircleView extends DebugView {

    public CircleView(GameObject gameObject) {
        super(gameObject, 0x0000FF);
    }

    public CircleView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    @Override
    public void render(Graphics g) {
        Circle c = (Circle) gameObject.getShape();
        g.setColor(new Color(color));
        g.drawOval((int) (c.getCenter().x - c.getRadius()), (int) (c.getCenter().y - c.getRadius()), (int) (c.getRadius() * 2), (int) (c.getRadius() * 2));
    }
}
