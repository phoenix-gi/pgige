package net.phoenixgi.pgige.view;

import net.phoenixgi.pgige.object.GameObject;

import java.awt.*;

public class DebugView extends ObjectView {

    protected int color = 0x0000FF;

    public DebugView(GameObject gameObject, int color) {
        super(gameObject);
        this.color = color;
    }

    @Override
    public void render(Graphics g) {

    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }
}
