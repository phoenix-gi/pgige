package net.phoenixgi.pgige;

public abstract class Game {
    SceneManager sceneManager;
    boolean isFullScreen = false;

    public abstract Canvas getCanvas();

    public void setSceneManager(SceneManager sceneManager) {
        this.sceneManager = sceneManager;
    }

    public SceneManager getSceneManager() {
        return sceneManager;
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void setFullScreen(boolean isFullScreen) {
        if (this.isFullScreen != isFullScreen) {
            this.isFullScreen = isFullScreen;
            this.fullScreenChanged();
        }
    }

    public abstract void fullScreenChanged();
}
