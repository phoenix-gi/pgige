package net.phoenixgi.pgige;

/**
 *
 * @author phoenix-gi
 */
public class FPSCounter {

    long startTime = System.nanoTime();
    public int frames = 0;

    public void logFrame() {
        frames++;
        if (System.nanoTime() - startTime >= 1_000_000_000) {
            System.out.println("FPS: " + frames);
            frames = 0;
            startTime = System.nanoTime();
        }
    }
}
