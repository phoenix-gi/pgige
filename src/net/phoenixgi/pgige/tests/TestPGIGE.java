package net.phoenixgi.pgige.tests;

import net.phoenixgi.pgige.GameUpdater;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.GameWindow;
import net.phoenixgi.pgige.pc.JPanelCanvas;

public class TestPGIGE {
    public static void main(String[] args) {
        JPanelCanvas canvas = new JPanelCanvas(512, 288);
        GameWindow window = new GameWindow(canvas, "TestPGIGE");
        GameUpdater updater = new GameUpdater(window);
        SceneManager manager = new SceneManager();
        Scene scene = new TestInputScene(manager, window);
        manager.setScene(scene);
        window.setSceneManager(manager);
        window.showWindow();
        updater.start();
    }
}
