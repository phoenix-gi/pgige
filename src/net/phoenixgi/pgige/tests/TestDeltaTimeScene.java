package net.phoenixgi.pgige.tests;

import net.phoenixgi.pgige.Game;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.StandardInput;

import java.awt.*;
import java.awt.event.KeyEvent;

public class TestDeltaTimeScene extends Scene {

    int x = 0;
    int y = 0;

    public TestDeltaTimeScene(SceneManager sceneManager, Game game) {
        super(sceneManager, game);
    }

    @Override
    public void initialize() {
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_SPACE:
                        getSceneManager().setScene(new TestObjectsScene(getSceneManager(), getGame()));
                        break;
                }
            }
        });
        getGame().setFullScreen(true);
    }

    @Override
    public void update(float deltaTime) {
        x += (int) (100 * deltaTime);
        if (x > getWidth()) {
            x = 0;
            y += 10;
        }
        if (y > getHeight()) {
            y = 0;
        }
    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g2d = (Graphics2D) drawingContext;
        g2d.setColor(Color.BLACK);
        g2d.drawLine(0, getHeight(), getWidth(), 0);
        g2d.fillOval(x - 4, y - 4, 8, 8);
    }

    @Override
    public void destroy() {

    }
}
