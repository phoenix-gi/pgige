package net.phoenixgi.pgige.tests;

import net.phoenixgi.pgige.Game;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.StandardInput;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class TestInputScene extends Scene {

    int mouseX = 0;
    int mouseY = 0;

    int imageX = 20;
    int imageY = 20;
    BufferedImage image = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D ig = image.createGraphics();

    public TestInputScene(SceneManager manager, Game game) {
        super(manager, game);
    }

    @Override
    public void initialize() {
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_SPACE:
                        getSceneManager().setScene(new TestDeltaTimeScene(getSceneManager(), getGame()));
                        break;
                    case KeyEvent.VK_F11:
                        getGame().setFullScreen(!getGame().isFullScreen());
                        break;
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                mouseX = e.getX();
                mouseY = e.getY();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                mouseX = e.getX();
                mouseY = e.getY();
                if (e.getX() > imageX && e.getX() < imageX + image.getWidth()) {
                    if (e.getY() > imageY && e.getY() < imageY + image.getHeight()) {
                        ig.setColor(Color.GREEN);
                        ig.fillOval(e.getX() - imageX - 4, e.getY() - imageY - 4, 8, 8);
                    }
                }
            }
        });
    }

    @Override
    public void update(float deltaTime) {

    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g2d = (Graphics2D) drawingContext;
        g2d.setColor(Color.BLACK);
        g2d.drawLine(0, 0, getWidth(), getHeight());
        g2d.drawImage(image, imageX, imageY, null);
        g2d.fillOval(mouseX - 4, mouseY - 4, 8, 8);
    }

    @Override
    public void destroy() {

    }
}
