package net.phoenixgi.pgige.tests;

import net.phoenixgi.pgige.Game;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.Vector2;
import net.phoenixgi.pgige.object.*;
import net.phoenixgi.pgige.pc.StandardInput;
import net.phoenixgi.pgige.shape.CrossTester;
import net.phoenixgi.pgige.view.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class CrossTestScene extends Scene {
    GameObject[] objects;
    DebugView[] views;

    GameObject[] testObjects;
    DebugView[] testViews;

    int currentObject;

    public CrossTestScene(SceneManager sceneManager, Game game) {
        super(sceneManager, game);
        RectangleObject rectangle;
        RectangleView rectangleView;

        CircleObject circle;
        CircleView circleView;

        TriangleObject triangle;
        TriangleView triangleView;

        LineObject line;
        LineView lineView;

        rectangle = new RectangleObject(100, 50);
        rectangleView = new RectangleView(rectangle);

        triangle = new TriangleObject(new Vector2(-30, -30), new Vector2(30, -30), new Vector2(0, 60));
        triangleView = new TriangleView(triangle);

        line = new LineObject(100);
        lineView = new LineView(line);

        circle = new CircleObject(50);
        circleView = new CircleView(circle);

        currentObject = 0;
        objects = new GameObject[]{rectangle, triangle, line, circle,};
        views = new DebugView[]{rectangleView, triangleView, lineView, circleView};

        rectangle = new RectangleObject(100, 50);
        rectangleView = new RectangleView(rectangle);

        triangle = new TriangleObject(new Vector2(-30, -30), new Vector2(30, -30), new Vector2(0, 60));
        triangleView = new TriangleView(triangle);

        line = new LineObject(100);
        lineView = new LineView(line);

        circle = new CircleObject(50);
        circleView = new CircleView(circle);

        rectangle.getShape().rotate(60);
        triangle.getShape().rotate(60);
        line.getShape().rotate(60);
        rectangle.getShape().setCenter(new Vector2(60, 120));
        triangle.getShape().setCenter(new Vector2(160, 120));
        line.getShape().setCenter(new Vector2(260, 120));
        circle.getShape().setCenter(new Vector2(360, 120));
        testObjects = new GameObject[]{rectangle, triangle, line, circle,};
        testViews = new DebugView[]{rectangleView, triangleView, lineView, circleView};
    }

    @Override
    public void initialize() {
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_SPACE:
                        getSceneManager().setScene(new TestInputScene(getSceneManager(), getGame()));
                        break;
                    case KeyEvent.VK_A:
                        currentObject--;
                        if (currentObject < 0) {
                            currentObject = objects.length - 1;
                        }
                        break;
                    case KeyEvent.VK_D:
                        currentObject++;
                        if (currentObject > objects.length - 1) {
                            currentObject = 0;
                        }
                        break;
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                objects[currentObject].getShape().setCenter(new Vector2(e.getX(), e.getY()));
            }
        });
        getGame().setFullScreen(true);
    }

    @Override
    public void update(float deltaTime) {
        objects[currentObject].getShape().rotate(45 * deltaTime);
        for (GameObject testObject : testObjects) {
            testObject.getShape().rotate(45 * deltaTime);
        }
    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g2d = (Graphics2D) drawingContext;

        for (DebugView testView : testViews) {
            if(CrossTester.isCross(testView.getGameObject().getShape(), objects[currentObject].getShape())) {
                testView.setColor(0xFF0000);
            } else {
                testView.setColor(0x0000FF);
            }
            testView.render(g2d);
        }
        views[currentObject].render(g2d);
    }

    @Override
    public void destroy() {

    }
}
